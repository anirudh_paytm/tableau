import { makeSchema, objectType, asNexusMethod } from "nexus";
import { DateTimeResolver } from "graphql-scalars";
import path from "path";

export const DateTime = asNexusMethod(DateTimeResolver, "date");

const Query = objectType({
  name: "Query",
  definition(t) {
    t.field("status", {
      type: objectType({
        name: "Status",
        definition(t) {
          t.boolean("up");
        }
      }),
      resolve: (_parent, _args) => {
        return { up: true };
      }
    });
  }
});

// const Mutation = objectType({
//   name: "Mutation",
//   definition(t) {},
// });

const User = objectType({
  name: "User",
  definition(t) {
    t.nonNull.int("id");
    t.string("name");
    t.nonNull.string("email");
  }
});

export const schema = makeSchema({
  types: [Query, /*Mutation,*/ User, DateTime],
  outputs: {
    schema: path.resolve(__dirname, "../../schema.graphql"),
    typegen: path.resolve(__dirname, "../generated/nexus.ts")
  },
  contextType: {
    module: require.resolve("../context"),
    export: "Context"
  }
});
