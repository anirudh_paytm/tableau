import * as Sentry from "@sentry/node";

export interface Context {}

export const context: Context = {};
