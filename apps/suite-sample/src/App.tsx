import React, { useState } from "react";
import { ChakraProvider, theme } from "@chakra-ui/react";

import logo from "./logo.svg";
import "./App.css";

// @ts-ignore
import { Thing, Button } from "@shared/components";

function App() {
  const [count, setCount] = useState(0);
  const [status, setStatus] = useState("loading");

  React.useEffect(() => {
    fetch("/api")
      .then(() => setStatus("success"))
      .catch(() => setStatus("error"));
  }, []);

  return (
    <ChakraProvider theme={theme} resetCSS={true}>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>Hello Vite + React!</p>
          <p>
            <Button size="lg" onClick={() => setCount((count) => count + 1)}>
              count is: {count}
            </Button>
          </p>
          <div> Gateway API ping status: {status}</div>
          <p>
            Edit <code>App.tsx</code> and save to test HMR updates.
          </p>
          <p>
            <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
              Learn React
            </a>
            {" | "}
            <a
              className="App-link"
              href="https://vitejs.dev/guide/features.html"
              target="_blank"
              rel="noopener noreferrer"
            >
              Vite Docs
            </a>
          </p>
        </header>
      </div>
    </ChakraProvider>
  );
}

export default App;
