import React from 'react';
import { Meta } from '@storybook/react';
import { Button, ButtonVariant } from '../src';

export default {
  title: 'UI',
  component: Button,
  argTypes: {
    variant: {
      control: { type: 'select' },
      options: ButtonVariant,
    },
  },
} as Meta;

const Template = (args) => (
  <>
    <Button {...args} size={'xs'}>
      {args.children} xs
    </Button>
    <Button {...args} size={'sm'}>
      {args.children} sm
    </Button>
    <Button {...args} size={'md'}>
      {args.children} md
    </Button>
    <Button {...args} size={'lg'}>
      {args.children} lg
    </Button>
  </>
);
export const Buttons = Template.bind({});
Buttons.args = {
  children: 'Button',
  variant: 'solid',
};
