import React, { FC, ReactChild } from 'react';
// @ts-ignore
import { Button as ChakraButton } from '@chakra-ui/react';

export enum ButtonVariant {
  solid = 'solid',
  outline = 'outline',
  ghost = 'ghost',
  link = 'link',
}

export interface ButtonProps {
  size?: string;
  variant?: ButtonVariant;
  children?: ReactChild | any; //any?
  onClick?: Function;
}
export const Button: FC<ButtonProps> = ({
  size = 'md',
  children,
  variant = 'solid',
  onClick = () => {},
}) => (
  <ChakraButton
    onClick={() => onClick()}
    colorScheme="blue"
    variant={variant}
    size={size}
    mx={1}
  >
    {children}
  </ChakraButton>
);
