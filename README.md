# Tableau

## Setup

- Install rush globally `npm install -g @microsoft/rush`
- Clone and run `rush install && rush build` - This will install all dependencies and build the project

> Avoid using package manager (`npm` /`yarn`) commands that install/link dependencies

## **Suites**

All suites are under the `apps` directory.

### Running in development mode

To start a suite locally, run `rushx dev` (for vite apps) or `rushx start` in the suite directory

### Adding a new Suite

- Add a new suite under `./apps` in a new directory
- Create an entry in `rush.json` under `"projects"` for the suite
- Run `rush update`.
- Run `rush rebuild`

### Adding an npm package

Run `rush add -p <package_name>`

## **Components**

Shared component library with storybook. Projects under `./shared/components`. Should be referred as `@shared/components` by suites

### Running locally

- Run storybook locally with `rushx storybook`
- Build components with `rushx build`. This is will publish the updated package locally

### Adding commponent library to a suite

Run `rush add -p @shared/components` in your suite directory
